import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PersonasEntity } from '../entities/personas.entity';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { PersonaDTO, PersonaUpdateDTO } from '../dto/persona.dto';
import { ErrorManager } from 'src/utils/error.manager';

@Injectable()
export class PersonasService {
    constructor(@InjectRepository(PersonasEntity) private readonly personaRepository: Repository<PersonasEntity>) { }

    public async createPersona(body: PersonaDTO): Promise<PersonasEntity> {
        try {
            return await this.personaRepository.save(body)
        } catch (error) {
            throw new Error(error)
        }
    }

    public async findPersona(): Promise<PersonasEntity[]> {
        try {
            const personas: PersonasEntity[] = await this.personaRepository.find()
            if (personas.length === 0) {
                throw new ErrorManager({
                    type: 'BAD_REQUEST',
                    message: 'No se encontro resultados'
                })
            }
            return personas
        } catch (error) {
            throw ErrorManager.createSignatureError(error.message)
        }
    }

    public async findPersonaById(idPersonas: number): Promise<PersonasEntity> {
        try {
            const persona: PersonasEntity = await this.personaRepository.createQueryBuilder('persona').where({idPersonas}).getOne()
            if(!persona) {
                throw new ErrorManager({
                    type: 'BAD_REQUEST',
                    message: 'No se encontro resultado'
                })
            }
            return persona
        } catch (error) {
            throw ErrorManager.createSignatureError(error.message)
        }
    }

    public async updatePersona(body: PersonaUpdateDTO, idPersonas: number): Promise<UpdateResult | undefined> {
        try {
            const persona: UpdateResult = await this.personaRepository.update(idPersonas, body)
            if (persona.affected === 0) {
                throw new ErrorManager({
                    type: 'BAD_REQUEST',
                    message: 'No se pudo actualizar'
                })
            }
            return persona
        } catch (error) {
            throw ErrorManager.createSignatureError(error.message)
        }
    }

    public async deletePersona(idPersonas: number): Promise<DeleteResult | undefined> {
        try {
            const persona: DeleteResult = await this.personaRepository.delete(idPersonas)
            if(persona.affected === 0) {
                throw new ErrorManager({
                    type: 'BAD_REQUEST',
                    message: 'No se pudo borrar'
                })
            }
            return persona
        } catch (error) {
            throw ErrorManager.createSignatureError(error.message)
        }
    }

}
