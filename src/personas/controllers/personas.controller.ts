import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { PersonasService } from '../services/personas.service';
import { PersonaDTO, PersonaUpdateDTO } from '../dto/persona.dto';

@Controller('personas')
export class PersonasController {
    constructor(private readonly personasService: PersonasService) {}

    @Post('resgistro')
    public async registroPersona(@Body() body: PersonaDTO) {
        return await this.personasService.createPersona(body)
    }

    @Get('all')
    public async findAllPersonas() {
        return await this.personasService.findPersona()
    }

    @Get(':id')
    public async findPersonaById(@Param('id') id: number) {
        return await this.personasService.findPersonaById(id)
    }

    @Put('edit/:id')
    public async updatePersona(@Param('id') id: number, @Body() body: PersonaUpdateDTO) {
        return await this.personasService.updatePersona(body, id)
    }

    @Delete('delete/:id')
    public async deletePersona(@Param('id') id: number) {
        return await this.personasService.deletePersona(id)
    }
    
}
