import { Module } from '@nestjs/common';
import { PersonasService } from './services/personas.service';
import { PersonasController } from './controllers/personas.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PersonasEntity } from './entities/personas.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([PersonasEntity])
  ],
  providers: [PersonasService],
  controllers: [PersonasController]
})
export class PersonasModule {}
