import { BaseEntity } from "src/config/base.entity";
import { IPersona } from "src/interfaces/personas.interfaca";
import { Column, Entity, PrimaryGeneratedColumn, BeforeInsert, OneToOne } from "typeorm";
import { UsuariosEntity } from "src/usuarios/entities/usuarios.entity";

@Entity('persona')
export class PersonasEntity extends BaseEntity implements IPersona {
    @PrimaryGeneratedColumn()
    idPersonas: number;

    @Column()
    nombres: string;

    @Column()
    ap_paterno: string;

    @Column()
    ap_materno: string;

    @Column()
    activo: boolean;

    // @OneToOne(()=>UsuariosEntity, (idPersona)=>UsuariosEntity.idPersona)

}