import { IsBoolean, IsNotEmpty, IsOptional, IsString } from "class-validator";

export class PersonaDTO {
    @IsNotEmpty()
    @IsString()
    nombres: string;

    @IsNotEmpty()
    @IsString()
    ap_paterno: string;

    @IsNotEmpty()
    @IsString()
    ap_materno: string;

    @IsNotEmpty()
    @IsBoolean()
    activo: boolean;
}

export class PersonaUpdateDTO {
    @IsOptional()
    @IsString()
    nombre: string;

    @IsOptional()
    @IsString()
    ap_paterno: string;

    @IsOptional()
    @IsString()
    ap_materno: string;

    @IsOptional()
    @IsBoolean()
    activo: boolean;
}