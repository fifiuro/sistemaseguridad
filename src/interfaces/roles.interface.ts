export interface IRol {
    rol: string;
    activo: boolean;
}