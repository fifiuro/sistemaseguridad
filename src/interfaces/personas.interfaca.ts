export interface IPersona {
    nombres: string;
    ap_paterno: string;
    ap_materno: string;
    activo: boolean;
}