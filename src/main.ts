import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService)
  const logger = new Logger('bootstrap');
  app.setGlobalPrefix('api/security/v1');
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors();

  await app.listen(
    process.env.NODE_ENV === 'DEVELOPMENT'
      ? configService.get('PORT')
      : configService.get('PORT_PROD'),
    () => {
      if (process.env.NODE_ENV === 'DEVELOPMENT') {
        logger.log(
          `API-SisAdmin Ready: Development on Line! on PORT: ${process.env.PORT}`,
        );
      } else {
        logger.log(
          `API-SisAdmin Ready: Production on Line! on PORT: ${process.env.PORT_PROD}`,
        );
      }
    },
  );
}
bootstrap();
