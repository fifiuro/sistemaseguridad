import { BaseEntity } from "src/config/base.entity";
import { IRol } from "src/interfaces/roles.interface";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('roles')
export class RolesEntity extends BaseEntity implements IRol {
    @PrimaryGeneratedColumn()
    idRoles: number;

    @Column()
    rol: string;

    @Column()
    activo: boolean;
}