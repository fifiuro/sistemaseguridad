import { IsBoolean, IsNotEmpty, IsOptional, IsString } from "class-validator";

export class RolDTO  {
    @IsNotEmpty()
    @IsString()
    rol: string;
    
    @IsNotEmpty()
    @IsBoolean()
    activo: boolean;
}

export class RolUpdateDTO  {
    @IsOptional()
    @IsString()
    rol: string;
    
    @IsOptional()
    @IsBoolean()
    activo: boolean;
}