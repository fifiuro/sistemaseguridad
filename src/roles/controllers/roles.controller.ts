import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { RolesService } from '../services/roles.service';
import { RolDTO, RolUpdateDTO } from '../dto/rol.dto';

@Controller('roles')
export class RolesController {
    constructor(private readonly rolesService: RolesService) { }

    @Post('registro')
    public async registroRol(@Body() body: RolDTO) {
        return await this.rolesService.createRol(body)
    }

    @Get('todo')
    public async findAllRoles() {
        return await this.rolesService.findRol()
    }

    @Get(':id')
    public async findRolById(@Param('id') id: number) {
        return await this.rolesService.findRolById(id)
    }

    @Put('modificar/:id')
    public async updateRol(@Param('id') id: number, @Body() body: RolUpdateDTO) {
        return await this.rolesService.updateRol(body, id)
    }

    @Delete('borrar/:id')
    public async deleteRol(@Param('id') id: number) {
        return await this.rolesService.deleteRol(id)
    }
}
