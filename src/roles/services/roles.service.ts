import { Injectable } from "@nestjs/common";
import { InjectRepository } from '@nestjs/typeorm';
import { RolesEntity } from "../entities/roles.entity";
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { RolDTO, RolUpdateDTO } from "../dto/rol.dto";
import { ErrorManager } from 'src/utils/error.manager';

@Injectable()
export class RolesService {
    constructor(@InjectRepository(RolesEntity) private readonly rolRepository: Repository<RolesEntity>) { }

    public async createRol(body: RolDTO): Promise<RolesEntity> {
        try {
            return await this.rolRepository.save(body)
        } catch (error) {
            throw new Error(error)
        }
    }

    public async findRol(): Promise<RolesEntity[]> {
        try {
            const roles: RolesEntity[] = await this.rolRepository.find()
            if (roles.length === 0) {
                throw new ErrorManager({
                    type: 'BAD_REQUEST',
                    message: 'No se encontro resultados'
                })
            }
            return roles
        } catch (error) {
            throw ErrorManager.createSignatureError(error.message)
        }
    }

    public async findRolById(idRoles: number): Promise<RolesEntity> {
        try {
            const roles: RolesEntity = await this.rolRepository.createQueryBuilder('roles').where({idRoles}).getOne()
            if(!roles) {
                throw new ErrorManager({
                    type: 'BAD_REQUEST',
                    message: 'No se encontro resultado'
                })
            }
            return roles
        } catch (error) {
            throw ErrorManager.createSignatureError(error.message)
        }
    }

    public async updateRol(body: RolUpdateDTO, idRoles: number): Promise<UpdateResult | undefined> {
        try {
            const rol: UpdateResult = await this.rolRepository.update(idRoles, body)
            if (rol.affected === 0) {
                throw new ErrorManager({
                    type: 'BAD_REQUEST',
                    message: 'No se pudo actualizar'
                })
            }
            return rol
        } catch (error) {
            throw ErrorManager.createSignatureError(error.message)
        }
    }

    public async deleteRol(idRoles: number): Promise<DeleteResult | undefined> {
        try {
            const rol: DeleteResult = await this.rolRepository.delete(idRoles)
            if(rol.affected === 0) {
                throw new ErrorManager({
                    type: 'BAD_REQUEST',
                    message: 'No se pudo borrar'
                })
            }
            return rol
        } catch (error) {
            throw ErrorManager.createSignatureError(error.message)
        }
    }

}