import { IsBoolean, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";
import { PersonasEntity } from "src/personas/entities/personas.entity";

export class UsuarioDTO {
    @IsNotEmpty()
    @IsString()
    usuario: string;

    @IsNotEmpty()
    @IsString()
    conbtrasenia: string;

    @IsNotEmpty()
    @IsBoolean()
    activo:boolean;

    @IsNotEmpty()
    @IsNumber()
    idPersonas: number;
}

export class UsusarioUpdateDTO {
    @IsOptional()
    @IsString()
    usuario: string;

    @IsOptional()
    @IsString()
    conbtrasenia: string;

    @IsOptional()
    @IsBoolean()
    activo:boolean;

    @IsOptional()
    @IsNumber()
    idPersonas: number;
}