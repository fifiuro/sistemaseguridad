import { Exclude } from "class-transformer";
import { BaseEntity } from "src/config/base.entity";
import { IUsuario } from "src/interfaces/usuarios.interface";
import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { PersonasEntity } from "src/personas/entities/personas.entity";

@Entity({ name: 'usuarios' })
export class UsuariosEntity extends BaseEntity implements IUsuario {
    @PrimaryGeneratedColumn()
    idUsuario: number;
    
    @Column()
    usuario: string;

    @Exclude()
    @Column()
    conbtrasenia: string;

    @Column()
    activo: boolean;

    @Column()
    idPersonas: number

    // @OneToOne(()=> PersonasEntity, PersonasEntity => PersonasEntity.idPersonas)
    @OneToOne(()=> PersonasEntity)
    @JoinColumn()
    id_personas: PersonasEntity;
    // @JoinColumn()
    // idPersona:PersonasEntity

}