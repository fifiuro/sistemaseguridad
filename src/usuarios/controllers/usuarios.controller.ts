import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { UsuariosService } from '../services/usuarios.service';
import { UsuarioDTO, UsusarioUpdateDTO } from '../dto/usuario.dto';

@Controller('usuarios')
export class UsuariosController {
    constructor(private readonly usuariosService: UsuariosService) {}

    @Post('registro')
    public async registroPersona(@Body() body: UsuarioDTO) {
        return await this.usuariosService.createUsuario(body)
    }

    @Get('all')
    public async findAllPersonas() {
        return await this.usuariosService.findUsuarios()
    }

    @Get(':id')
    public async findPersonaById(@Param('id') id: number) {
        return await this.usuariosService.findUsuarioById(id)
    }

    @Put('edit/:id')
    public async updatePersona(@Param('id') id: number, @Body() body: UsusarioUpdateDTO) {
        return await this.usuariosService.updateUsuario(body, id)
    }

    @Delete('delete/:id')
    public async deletePersona(@Param('id') id: number) {
        return await this.usuariosService.deleteUsuario(id)
    }

}
