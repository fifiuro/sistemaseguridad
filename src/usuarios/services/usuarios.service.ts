import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { UsuariosEntity } from '../entities/usuarios.entity';
import { UsuarioDTO, UsusarioUpdateDTO } from '../dto/usuario.dto';
import { ErrorManager } from 'src/utils/error.manager';

@Injectable()
export class UsuariosService {
    constructor(@InjectRepository(UsuariosEntity) private readonly usuarioRepository: Repository<UsuariosEntity>) { }

    public async createUsuario(body: UsuarioDTO): Promise<UsuariosEntity> {
        try {
            return await this.usuarioRepository.save(body)
        } catch (error) {
            throw new Error(error)
        }
    }

    public async findUsuarios(): Promise<UsuariosEntity[]> {
        try {
            const usuarios: UsuariosEntity[] = await this.usuarioRepository.find()
            if (usuarios.length === 0) {
                throw new ErrorManager({
                    type: 'BAD_REQUEST',
                    message: 'No se encontro resultados'
                })
            }
            return usuarios
        } catch (error) {
            throw ErrorManager.createSignatureError(error.message)
        }
    }

    public async findUsuarioById(idUsuario: number): Promise<UsuariosEntity> {
        try {
            const usuario: UsuariosEntity = await this.usuarioRepository.createQueryBuilder('usuarios').where({idUsuario}).leftJoinAndSelect('usuarios.idPersonas','persona').getOne()
            if(!usuario) {
                throw new ErrorManager({
                    type: 'BAD_REQUEST',
                    message: 'No se encontro resultado'
                })
            }
            return usuario
        } catch (error) {
            throw ErrorManager.createSignatureError(error.message)
        }
    }

    public async updateUsuario(body: UsusarioUpdateDTO, idUsuario: number): Promise<UpdateResult | undefined> {
        try {
            const usuario: UpdateResult = await this.usuarioRepository.update(idUsuario, body)
            if (usuario.affected === 0) {
                throw new ErrorManager({
                    type: 'BAD_REQUEST',
                    message: 'No se pudo actualizar'
                })
            }
            return usuario
        } catch (error) {
            throw ErrorManager.createSignatureError(error.message)
        }
    }

    public async deleteUsuario(idUsuario: number): Promise<DeleteResult | undefined> {
        try {
            const usuario: DeleteResult = await this.usuarioRepository.delete(idUsuario)
            if(usuario.affected === 0) {
                throw new ErrorManager({
                    type: 'BAD_REQUEST',
                    message: 'No se pudo borrar'
                })
            }
            return usuario
        } catch (error) {
            throw ErrorManager.createSignatureError(error.message)
        }
    }

}
