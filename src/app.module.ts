import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { PersonasModule } from './personas/personas.module';
import { DataSourceConfig } from './config/data.source';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RolesModule } from './roles/roles.module';
import { UsuariosModule } from './usuarios/usuarios.module';

@Module({
  imports: [ConfigModule.forRoot({ envFilePath: `.env.base`, isGlobal: true }), TypeOrmModule.forRoot({ ...DataSourceConfig }), PersonasModule, RolesModule, UsuariosModule],
  controllers: [],
  providers: [],
})
export class AppModule { }
